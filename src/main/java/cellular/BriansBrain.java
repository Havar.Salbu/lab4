package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {


    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row,column);
    }

    @Override
    public void initializeCells() {
		Random random = new Random();
        int rand;
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
                rand = random.nextInt(3);
				if (rand == 0) {
					currentGeneration.set(row, col, CellState.ALIVE);
				}
                else if(rand == 1) {
					currentGeneration.set(row, col, CellState.DYING);
				}
                else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
			}
		}
    }

    @Override
    public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for(int r=0; r<numberOfRows(); r++) {
			for(int c=0; c<numberOfColumns(); c++) {
				nextGeneration.set(r, c, getNextCell(r, c));
			}
		}
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        if(getCellState(row, col) == CellState.ALIVE)
            return CellState.DYING;
        else if(getCellState(row, col) == CellState.DYING)
            return CellState.DEAD;
        else if(getCellState(row, col) == CellState.DEAD && countNeighbors(row,col,CellState.ALIVE) == 2)
            return CellState.ALIVE;
        else
            return CellState.DEAD;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	public int countNeighbors(int row, int col, CellState state) {
		int num_alive_neighbors = 0;
		if(row==0 && col==0) {
			num_alive_neighbors += (getCellState(row,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col+1) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 3 - num_alive_neighbors;
		}
		else if(row==0 && col<numberOfColumns()-1) {
			num_alive_neighbors += (getCellState(row,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col+1) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 5 - num_alive_neighbors;
		}
		else if(row==0 && col == numberOfColumns()-1) {
			num_alive_neighbors += (getCellState(row,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col-1) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 3 - num_alive_neighbors;
		}
		else if(row<numberOfRows()-1 && col==0) {
			num_alive_neighbors += (getCellState(row-1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 5 - num_alive_neighbors;
		}
		else if(row==numberOfRows()-1 && col==0) {
			num_alive_neighbors += (getCellState(row-1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row,col+1) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 3 - num_alive_neighbors;
		}
		else if(row==numberOfRows()-1 && col<numberOfColumns()-1) {
			num_alive_neighbors += (getCellState(row,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row,col+1) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 5 - num_alive_neighbors;
		}
		else if(row==numberOfRows()-1 && col==numberOfColumns()-1) {
			num_alive_neighbors += (getCellState(row,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 3 - num_alive_neighbors;
		}
		else if(row<numberOfRows()-1 && col==numberOfColumns()-1) {
			num_alive_neighbors += (getCellState(row+1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 5 - num_alive_neighbors;
		}
		else {
			num_alive_neighbors += (getCellState(row-1,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row-1,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col+1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row+1,col-1) == CellState.ALIVE ? 1 : 0);
			num_alive_neighbors += (getCellState(row,col-1) == CellState.ALIVE ? 1 : 0);
			return state == CellState.ALIVE ? num_alive_neighbors : 8 - num_alive_neighbors;
		}
	}


    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    
}
