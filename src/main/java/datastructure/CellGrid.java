package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private final int rows;
    private final int cols;
    private CellState[][] cellstates;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        cellstates = new CellState[rows][columns];
        for(int r=0; r<rows; r++) {
            for(int c=0; c<columns; c++) {
                cellstates[r][c] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) throws IndexOutOfBoundsException{
        if(row >= 0 && row < rows && column >= 0 && column < cols) {
            cellstates[row][column] = element;
        }
        else throw new IndexOutOfBoundsException();
    }

    @Override
    public CellState get(int row, int column) throws IndexOutOfBoundsException{
        if(row >= 0 && row < rows && column >= 0 && column < cols) {
            return cellstates[row][column];
        }
        else throw new IndexOutOfBoundsException();
    }

    @Override
    public IGrid copy() {
        CellGrid grid = new CellGrid(this.rows, this.cols, cellstates[0][0]);
        for(int r=0; r<numRows(); r++) {
            for(int c=0; c<numColumns(); c++) {
                grid.set(r,c,cellstates[r][c]);
            }
        }
        return grid;
    }
}
